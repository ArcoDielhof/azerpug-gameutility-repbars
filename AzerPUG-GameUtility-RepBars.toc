## Interface: 90005
## Title: |cFF00FFFFAZP|r |cFFFF00FFGU|r |cFFFFFF00RepBars|r
## Author: Tex & AzerPUG Gaming Community (www.azerpug.com/discord)
## Dependencies: AzerPUG-GameUtility-Core 
## Notes: Utility for the Game!
## Version: SL 9.0.5 (For actual addon version, check main.lua)
## SavedVariablesPerCharacter: AZPGURepBarsData

dataTables.lua
main.lua